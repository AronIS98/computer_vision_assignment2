#1
# import cv2
# import time
# import numpy as np
# import math

# def main():
#     cap = cv2.VideoCapture(0) # 0 is for the computer webcam, 1 is for the phone, https://www.e2esoft.com/ivcam/

#     while(True):
#         img = cap.read()[1]
#         # img = cv2.imread('drawnRectangle.png',0)
#         # img = cv2.imread('img_bjark1.png',1)
        
#         img = img[100:480, 0:640]
#         edges = cv2.Canny(img,100,200)
#         point_list = np.argwhere(edges != 0)
#         lengd = len(point_list)
#         best_n = 0
#         best_points = []
#         points_used = []
#         for _ in range(200):
#             templist = []
#             if(lengd<=0):
#                 break
#             random_index1 = np.random.randint(lengd)
#             random_index2 = np.random.randint(lengd)
#             point1 = point_list[random_index1]
#             point2 = point_list[random_index2]
#             n = 0
#             for i in range(0,lengd,10):
                
#                 point = point_list[i]
#                 if((i != random_index1) and (i != random_index2)):
#                     d = np.linalg.norm(np.cross(point2-point1, point1-point))/np.linalg.norm(point2-point1)
#                     if(d < 5):
#                         n +=1
#                         templist.append(point)
#             if(n > best_n):
#                 best_n = n
#                 best_points = [point1,point2]
#                 points_used = templist

#         points_used = np.array(points_used)
#         draw_x = np.arange(0, edges.shape[1], 1)
#         draw_y = np.polyval(np.polyfit(points_used[:,1],points_used[:,0],1),draw_x)
#         draw_points = (np.asarray([draw_x, draw_y]).T).astype(np.int32) 

#         cv2.polylines(img, [draw_points], False, (0,0,255),2) 
#         # # points_used = [x for x in points_used]
#         # # print(points_used)
#         # punktur1 = tuple(reversed(best_points[0]))
#         # punktur2 = tuple(reversed(best_points[1]))
#         # img = cv2.circle(img, punktur1, radius=1, color=(0, 0, 255), thickness=2)
#         # img = cv2.circle(img, punktur2, radius=1, color=(0, 0, 255), thickness=2)
#         # img = cv2.line(img, punktur1, punktur2, (0,0,255), 3, cv2.LINE_AA)
#         cv2.imshow("Detected Lines (in red) - Standard Hough Line Transform", img)
#         cv2.imshow('absde',edges)

#         if cv2.waitKey(1) & 0xFF == ord('q'):
#             break

# main()

# # 2
# import cv2
# import time
# import numpy as np
# import math
# from collections import defaultdict

# def segment_by_angle_kmeans(lines, k=2, **kwargs):
#     """Groups lines based on angle with k-means.

#     Uses k-means on the coordinates of the angle on the unit circle 
#     to segment `k` angles inside `lines`.
#     """

#     # Define criteria = (type, max_iter, epsilon)
#     default_criteria_type = cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER
#     criteria = kwargs.get('criteria', (default_criteria_type, 10, 1.0))
#     flags = kwargs.get('flags', cv2.KMEANS_RANDOM_CENTERS)
#     attempts = kwargs.get('attempts', 10)

#     # returns angles in [0, pi] in radians
#     angles = np.array([line[0][1] for line in lines])
#     # multiply the angles by two and find coordinates of that angle
#     pts = np.array([[np.cos(2*angle), np.sin(2*angle)]
#                     for angle in angles], dtype=np.float32)

#     # run kmeans on the coords
#     labels, centers = cv2.kmeans(pts, k, None, criteria, attempts, flags)[1:]
#     labels = labels.reshape(-1)  # transpose to row vec

#     # segment lines based on their kmeans label
#     segmented = defaultdict(list)
#     for i, line in enumerate(lines):
#         segmented[labels[i]].append(line)
#     segmented = list(segmented.values())
#     return segmented

# def intersection(line1, line2):
#     """Finds the intersection of two lines given in Hesse normal form.

#     Returns closest integer pixel locations.
#     See https://stackoverflow.com/a/383527/5087436
#     """
#     rho1, theta1 = line1[0]
#     rho2, theta2 = line2[0]
#     A = np.array([
#         [np.cos(theta1), np.sin(theta1)],
#         [np.cos(theta2), np.sin(theta2)]
#     ])
#     b = np.array([[rho1], [rho2]])
#     x0, y0 = np.linalg.solve(A, b)
#     x0, y0 = int(np.round(x0)), int(np.round(y0))
#     return [[x0, y0]]


# def segmented_intersections(lines):
#     """Finds the intersections between groups of lines."""

#     intersections = []
#     for i, group in enumerate(lines[:-1]):
#         for next_group in lines[i+1:]:
#             for line1 in group:
#                 for line2 in next_group:
#                     intersections.append(intersection(line1, line2)) 

#     return intersections






# def main():
#     cap = cv2.VideoCapture(1) # 0 is for the computer webcam, 1 is for the phone, https://www.e2esoft.com/ivcam/

#     # frame = frame[100:480, 0:640]
#     while(True):
#         # frame = cv2.imread('drawnRectangle.png',0)
#         # frame = cv2.imread('img_bjark1.png',0)
#         frame = cap.read()[1]
#         gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#         edges = cv2.Canny(gray,100,200)
#         line = cv2.HoughLines(edges, 1, np.pi / 180, 140)
#         lines_used = []
#         points_used = []
#         intersections = []
#         if line is not None:
#             for i in range(0, len(line)):
#                 rho = line[i][0][0]
#                 theta = line[i][0][1]
#                 lines_used.append((rho,theta))
#                 a = math.cos(theta)
#                 b = math.sin(theta)
#                 x0 = a * rho
#                 y0 = b * rho
#                 pt1 = (int(x0 + 1000*(-b)), int(y0 + 1000*(a)))
#                 pt2 = (int(x0 - 1000*(-b)), int(y0 - 1000*(a)))
#                 points_used.append(pt1)

#                 cv2.line(frame, pt1, pt2, (0,0,255), 2)
#             if len(line>2):
#                 segmented = segment_by_angle_kmeans(line)
#                 intersections = segmented_intersections(segmented)

#             if len(intersections) == 4:
#                 for inter in intersections:
#                     cv2.circle(frame, inter[0], radius=1, color=(0, 255, 0), thickness=8)
#                 intersections = np.array(([x[0] for x in intersections]))  
#                 new_corners =  np.float32([[0,0],[639,0],[0, 479],[639,479]])
#                 h, _ = cv2.findHomography(intersections, new_corners)
#                 im_out = cv2.warpPerspective(frame, h, (640,480))
#                 cv2.imshow("out_img",im_out)
#         cv2.imshow("Detected Lines (in red) - Standard Hough Line Transform", frame)
#         cv2.imshow("a", edges)
#         if cv2.waitKey(1) & 0xFF == ord('q'):
#             break
#     cap.release()
#     cv2.destroyAllWindows()

# main()

